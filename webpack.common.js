const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin")
const AbsolutePathProviderPlugin = require('abspath-webpack-plugin')
module.exports = {
  entry: {
    app: './src/index.js',
    vendor: ['react', 'react-dom']
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
        //loader: 'babel-loader',
        // exclude: /node_modules/,
        // query: {
        //     presets: ['es2015']
        // }
      },
      {
        test: /\.css$/,
        use: [
                'style-loader',
                'css-loader'
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  resolve: {
    alias: {
      'Sass': path.resolve(__dirname, 'src/sass/components'),
      'Components': path.resolve(__dirname, 'src/components'),
      'MainComponents': path.resolve(__dirname, 'src/dashboard/container'),
      'Utils': path.resolve(__dirname, 'src/utils'),
      //'@images': path.resolve(__dirname, 'images')
    }
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      title: 'Output Management',
      template: './index.html'
    }),
    new AbsolutePathProviderPlugin(/^@sass/, path.resolve('./src/sass')),
    new AbsolutePathProviderPlugin(/^@utils/, path.resolve('./src/utils')),
    new AbsolutePathProviderPlugin(/^@components/, path.resolve('./src/components')),
    new CompressionPlugin({  
      test: /\.jsx?$|\.js$|\.css$|\.html$/,
      filename: "[path].gz[query]",
      exclude: /node_modules/,
      algorithm: 'gzip',
      threshold: 10240,
      minRatio: 0.8
    })
  ],
  output: {
    filename: '[name].[chunkhash].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: 'http://businessdhronacharya.com/userPortal'
    //publicPath: "/"
  },
  optimization: {
    splitChunks: {
      chunks: 'async',
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '~',
      name: true,
    },
    runtimeChunk: true
  }
};