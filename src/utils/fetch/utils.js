/**
 * utility methods for constructing `Fetch` API
 */

import "whatwg-fetch"
import { Api } from "./../config"

function getHeaders(type) {
  const json_headers = {
    'Accept': 'application/json',
    //'Content-Type': 'application/json'
  }

  return Object.assign({}, json_headers)
}


/**
 * fetch data constructor(s)
 */
function constructBody({ type, data }) {
  switch (type) {
    case "FormData":
      return data

    default:
      return JSON.stringify(data)
  }
}

/**
 * Error handling helpers
 */
export function checkStatus(response) {
  if (response.status >= 200 && response.status < 305) {
    return response
  } else {
    // console.log(response.statusText);
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

/**
 * constructFetchUtility - return a window.fetch instance
 * @param {Object} options
 */
export function constructFetchUtility(options) {
  const { api, data, method, type, cors, prependBaseUrl = true, apiBase } = options

  // construct request url
  const url = prependBaseUrl ? `${Api[apiBase]}${api}` : api

  // construct options for creating `window.fetch` instance
  let fetchOptions = {
    method,
    headers: getHeaders(type),
  }

  if(cors) fetchOptions.mode = 'cors'
  // add data to request
  if (data && method !== "GET") {
    fetchOptions.body = constructBody({type, data})
  }
  console.log("fetch", fetchOptions)
  return (options.handleError)
    ? fetch(url, fetchOptions)
      .then(checkStatus)
      .then(parseJSON)
      // .then((data) => {
      //   console.log("data", data.json())
      //   return data.json()
      // })
    : fetch(url, fetchOptions)
      .then(parseJSON)
}

function parseJSON(response) {
  return response.json().then((data) => data)
}
