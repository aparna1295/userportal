export function createSession(data) {
  localStorage.setItem("username", data.name)
  localStorage.setItem("password", data.password)
  localStorage.setItem("userDetails", JSON.stringify(data))
}

export function clearSession() {
  localStorage.clear()
}