import React from 'react'
import './label.scss'

const Label = ({ children, color, icon, tooltipText }) => {
  return (
    <div style={{display: 'flex', alignItems: 'center'}}>
      <p className="label" style={{marginRight: '10px'}}>
        { children }
      </p>
    </div>
  )
}

export default Label
