import React from 'react'
import './select.scss'
import Icon from './../icons'

class Select extends React.Component {
  constructor() {
    super()
    this.handleChange = this.handleChange.bind(this)
    this.state = {
      value: ""
    }
  }
  handleChange(e) {
    this.setState({value: e.target.value})
    this.props.onChange(e)
  }
  render() {
    return (
      <div className="select--container">
        <Icon name="down-small" size="10"/>
        <select
          placeholder={this.props.placeholder}
          className={`select ${this.props.small ? 'small' : ''}`}
          name={this.props.name}
          onChange={this.handleChange}
          value={this.props.value ? this.props.value : this.state.value}
        >
        
            <option value="" disabled selected>
              Choose a email template
            </option>
          
          {
            this.props.options.map((item, i) => (
              <option key={i} value={item.value}>{ item.text }</option>
            ))
          }
      </select>
      </div>
    )
  }
}

export default Select