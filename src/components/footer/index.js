import React from "react"
import "./footer.scss"
import Icon from "./../icons"

class Footer extends React.Component {
  render() {
    return (
      <div id="footer">
        <div>
          <ul>
            <li><a target="_blank" href="" className="os s8">About Us</a></li>
            <li><a target="_blank" href="" className="os s8">Services</a></li>
            <li><a target="_blank" href="" className="os s8">Get In Touch</a></li>
            <li><a target="_blank" href="" className="os s8">Careers</a></li>
            <li><a target="_blank" href="" className="os s8">Work</a></li>
          </ul>
        </div>
        <div className="social-media-icons">
          <a target="_blank" href=""><Icon name="facebook" /></a>
          <a target="_blank" href=""><Icon name="instagram" /></a>
          <a target="_blank" href=""><Icon name="linkedin" /></a>
        </div>
        <div>
          <p className="os s7">Copyright 2019 Business Dhronacharya - All Rights Reserved</p>
        </div>
      </div>
    )
  }
}

export default Footer