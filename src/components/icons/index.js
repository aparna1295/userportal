import icons from "./icon"

const icon = ({name}) => {
  return icons[name]
}

export default icon 