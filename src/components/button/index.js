import React from 'react'
import './button.scss'

class Button extends React.Component {

  constructor() {
    super()

    this.getClassName = this.getClassName.bind(this)
  }

  getClassName() {
    let className = 'btn-default'
    if (this.props.primary) {
      className = 'btn-primary'
    } else if (this.props.secondary) {
      className = 'btn-secondary'
    } else if (this.props.danger) {
      className = 'btn-danger'
    } else if (this.props.google) {
      className = 'btn-google'
    } else if (this.props.facebook) {
      className = 'btn-facebook'
    } else if (this.props.logout) {
      className = 'btn-logout'
    }

    return className
  }

  render() {
    return (
      <button
        disabled={this.props.disabled}
        // style={this.props.style}
        onClick={this.props.onClick}
        className={`btn ${this.getClassName()}`
      }>
        { this.props.children }
      </button>
    )
  }
}

export default Button
