import React from "react"
import "./layout.scss"

class Layout extends React.Component {
  constructor() {
    super()
  }

  render() {
    return (
      <div className="layout">
        {this.props.children}
      </div>
    )
  }
}

export default Layout