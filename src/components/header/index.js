import React from "react";
import "./header.scss";
import Icon from "./../icons";
import {clearSession} from "Utils/session-utils"
import Button from "Components/button"
import {webBaseUrl} from "Utils/static-urls"
import SignIn from "MainComponents/SignIn"
import { unMountModal, mountModal } from 'Components/modal/utils'

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoggedIn: false
    } 
    this.logout = this.logout.bind(this)
  }

  componentDidMount() {
    document.addEventListener('click', this.handlePress)
    console.log("username", this.state.isLoggedIn, localStorage.getItem("username"))
    if(localStorage.getItem("username")) {
      this.setState({isLoggedIn: true})
    }
  }

  logout() {
    clearSession()
    this.setState({isLoggedIn: false})
    location.href=`${webBaseUrl}/`
    //mountModal(SignIn({}))
  }

  mountModal() {
    mountModal(SignIn({}))
  }

  // componentDidMount() {
  //   document.addEventListener('click', this.handlePress)
  // }

  componentWillUnmount() {
    document.removeEventListener('click', this.handlePress)
  }

  handlePress(e) {
    if (e.target.className === 'overlay-container') {
      //this.props.onClick()
      console.log("erer")
    }
  }

  render() {
    const {isLoggedIn} = this.state
    return (
      <div id="pageHeader" className="page-header">
        <div className="logo">
          <img src="./images/logo.jpg" />
          {
            isLoggedIn &&
            // <span onClick={this.logout}>
            //   <Icon name="logout" />
            // </span>
            <Button primary onClick={this.logout}>Logout</Button>
          }
          {
            !isLoggedIn &&
            <Button primary onClick={this.mountModal}>Login</Button>
          }
        </div>
      </div>
    );
  }
}

export default Header;
