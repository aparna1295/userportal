import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import { Provider } from 'react-redux'
import Dashboard from './dashboard'
import configureStore from './dashboard/store/configureStore'
import rootSaga from './dashboard/middleware/saga'
import Header from 'Components/header'
import Footer from 'Components/footer'
import Landing from "./dashboard/container/landing"
import PaidUserLanding from './dashboard/container/paidUserLanding';
import PaidUserCurrentCourse from "./dashboard/container/paidUserCurrentCourse"
import ThankyouNote from "./dashboard/container/thankyouNote"
import NonPaidUser from "./dashboard/container/nonPaidUser"
import CourseList from "./dashboard/container/course-list"
import Profile from "./dashboard/container/profile"
import Layout from 'Components/layout';
import {webBaseUrl} from "Utils/static-urls"
import CourseDetail from "./dashboard/container/course-detail"

const config = configureStore()
config.runSaga(rootSaga)

class App extends React.Component {
  componentDidMount() {
    if(localStorage.getItem("userDetails") && JSON.parse(localStorage.getItem("userDetails")).isPaidUser === "0") {
      if(location.pathname === `${webBaseUrl}/` && location.pathname !== `${webBaseUrl}/non-paid-user`) {
        location.href = `${webBaseUrl}/non-paid-user`
      }
    } else if(location.pathname !== "/") {
      location.href = `${webBaseUrl}/`
    }
  }
  render() {
    return (
      <Provider store={config.store}>
        <Router basename={`${webBaseUrl}`}  history={history}>
          <div style={{
            backgroundColor: '#fff',
            width: '100%',
            //maxWidth: '1440px',
            margin: '0 auto',
            height: '100vh',
            overflow: 'auto'
          }}>
            <Header />
            <Route exact path='/' component={Landing} />
            <Route exact path='/non-paid-user-profile' component={Profile} />
            {/* <Route exact path='/paid-user' component={PaidUserLanding} /> */}
            {/* <Route exact path='/paid-user-current-course' component={PaidUserCurrentCourse} /> */}
            <Route exact path='/non-paid-user' component={NonPaidUser} />
            <Route exact path='/course-list' component={CourseList} />
            <Route exact path='/course-detail/:courseId' component={CourseDetail} />
            <Route exact path='/success-enrollment' component={ThankyouNote} />
            <Footer />
            {/* <Switch>
              <Route exact path="/home" component={Landing} />
            </Switch> */}
          </div>
        </Router>
      </Provider>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'))

export default App

