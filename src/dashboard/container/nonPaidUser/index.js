import React from "react"
import Icon from "Components/icons"
import "./nonPaidUser.scss"
import {webBaseUrl} from "Utils/static-urls"

class NonPaidUser extends React.Component {
  constructor() {
    super()
    this.state = {
      username: ''
    }
    this.handleCourseClick = this.handleCourseClick.bind(this)
  }

  componentDidMount() {
    this.setState({username: localStorage.getItem("username")})
  }

  handleCourseClick() {
    location.href=`${webBaseUrl}/course-list`
  }

  handleProfileClick() {
    location.href=`${webBaseUrl}/non-paid-user-profile`
  }

  render() {
    return (
      <div id="nonPaidUserLanding" style={{backgroundImage: 'url(./images/paid-user.jpg)'}}>
        <div className="content">
          <h2 className="os s1">Welcome {this.state.username}</h2>
          <h3 className="os s1">Welcome to Business Dhronacharya What would you like to do today</h3>
          <div className="user-profile">
            <div onClick={this.handleProfileClick}>
              <Icon name="profile" />
              <p>Your Profile</p>
            </div>
            <div onClick={this.handleCourseClick}>
              <Icon name="courses" />
              <p>Courses</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default NonPaidUser