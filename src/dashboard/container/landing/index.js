import React from "react"
import './landing.scss'
//import Vimeo from "vimeo-reactjs"
import Vimeo from "react-vimeo";
import { mountModal } from "Components/modal/utils";
import SignIn from "./../SignIn"
import Header from "Components/header"
import Modal from "Components/modal"
import Layout from "Components/layout";

class Landing extends React.Component {
  constructor() {
    super()
    this.state = {
      isLoggedIn: false
    }
    // this.testModal = this.testModal.bind(this)
  }

  componentDidMount() {
    console.log("mount")
    if(!localStorage.getItem("username")) {
      //mountModal(SignIn({}))
      this.setState({isLoggedIn: false})
    } else {
      this.setState({isLoggedIn: true})
    }
  }

  render() {
    const {isLoggedIn} = this.state
    return (
      <React.Fragment>
      <Layout>
      <div id="landing">
        <div className="section-1">
          <h2 className="os s0">Business Dhronacharya Institute of Entrepreneurship</h2>
          <p className="os s6">Our competency is to develop a mind-set of the aspiring candidates through our entrepreneurship expertiseto start new ventures and help it to translate into huge enterprises</p>
        </div>
      
        <div className="video">
          {
            !isLoggedIn &&
            <Vimeo videoId="309049643" autoplay={true}/>
          }
          {
            isLoggedIn &&
            <Vimeo videoId="309049643" autoplay={true}/>
          }
          {/* <Vimeo video="309049643" height="315px" width="540px" /> */}
        </div>
  
        <div>
          <h2 className="os s0">WHAT OUR FANTASTIC USERS SAY</h2>
          <p className="os s6">Have a look at what the entreprenurs say from their hearts</p>
        </div>
    
        <div className="co-workers">
          <div>
            <div className="img">
              <img src="./images/face1.jpg" />
            </div>
            <p className="description os s3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, aspernatur, voluptatibus, atque, tempore molestiae.</p>
            <p className="designation os s3">Software Engineer</p>
            <p className="os s3">John Smith</p>
          </div>
          <div>
            <div className="img">
              <img src="./images/face2.jpg" />
            </div>
            <p className="description os s3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, aspernatur, voluptatibus, atque, tempore molestiae.</p>
            <p className="designation os s3">Software Engineer</p>
            <p className="os s3">Beautician</p>
          </div>
          <div>
            <div className="img">
              <img src="./images/face3.jpg" />
            </div>
            <p className="description os s3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, aspernatur, voluptatibus, atque, tempore molestiae.</p>
            <p className="designation os s3">Software Engineer</p>
            <p className="os s3">Business Man</p>
          </div>
        </div>
      </div>
      </Layout>
      </React.Fragment>
    )
  }
}

export default Landing