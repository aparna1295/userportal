import React from "react"
import Label from "Components/label"
import Button from "Components/button"
import "Sass/form-input.scss"

class SignupForm extends React.Component {
  constructor() {
    super()

    this.state = {
      name: "",
      mobile: "",
      dob: "",
      location: "",
      referral: "",
      occupation: "",
      gender: "male",
      termsAndCondition: false,
      nameErr: {
        value: "",
        status: false
      },
      mobileErr: {
        value: "",
        status: false
      },
      dobErr: {
        value: "",
        status: false
      },
      locationErr: {
        value: "",
        status: false
      },
      occupationErr: {
        value: "",
        status: false
      },
      termsAndCondnErr: {
        value: "",
        status: false
      }
    }

    this.handleTextChange = this.handleTextChange.bind(this)
    this.handleGenderChange = this.handleGenderChange.bind(this)
    this.getData = this.getData.bind(this)
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this)
    this.handleChangeInMobile = this.handleChangeInMobile.bind(this)
    this.isFormValid = this.isFormValid.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  handleGenderChange(e) {
    console.log("name", e.target.name)
    this.setState({ [e.target.name]: e.target.value })
  }

  handleCheckboxChange() {
    const termsAndCondition = this.state.termsAndCondition
    this.setState({ termsAndCondition: !termsAndCondition})
  }

  handleChangeInMobile(e) {
    this.setState({
      mobileErr: {
        value: "",
        status: false
      }
    })
    const re = /^[0-9\b]*$/;
    if ((e.target.value === '' || re.test(e.target.value))) {
       this.setState({mobile: e.target.value})
    }
  }

  isFormValid() {
    if(this.state.name.length === 0) {
      this.setState({
        nameErr: {
          value: "Name is required",
          status: true
        }
      })
      return false;
    } else if(this.state.mobile.length === 0) {
      this.setState({
        mobileErr: {
          value: "Mobile is required",
          status: true
        }
      })
      return false;
    } else if(this.state.dob.length === 0) {
      this.setState({
        dobErr: {
          value: "Date of Birth is required",
          status: true
        }
      })
      return false;
    } else if(this.state.occupation.length === 0) {
      this.setState({
        occupationErr: {
          value: "Occupation is required",
          status: true
        }
      })
      return false;
    } else if(this.state.location.length === 0) {
      this.setState({
        locationErr: {
          value: "Location is required",
          status: true
        }
      })
      return false;
    } else if(!this.state.termsAndCondition) {
      this.setState({
        termsAndCondnErr: {
          value: "Please agree terms and conditions",
          status: true
        }
      })
      return false;
    }
    return true;
  }
  onSubmit() {
    if(this.isFormValid()) {
      this.props.onSubmit()
    }
  }

  handleTextChange(evt) {
    console.log("name", evt.target.name, evt.target.validity.valid)
    if((evt.target.validity.valid || evt.target.validity.valueMissing) && evt.target.value.trim().length) {
      this.setState({ [evt.target.name]: evt.target.value})
    } else { 
      evt.preventDefault()
    }
  }

  getData() {
    return this.state
  }

  render() {
    const {
      nameErr,
      mobileErr,
      occupationErr,
      dobErr,
      locationErr,
      termsAndCondnErr
    } = this.state
    return (
      // <form>
      <React.Fragment>
        <h2 className="os s2">
          Sign Up
        </h2>
        
        <div className="form">
          <div className="form-group">
            <Label>Name *</Label>
            <input
              type="text"
              name="name"
              pattern="^[A-Za-z0-9 ]*$"
              onInput={(e) => this.handleTextChange(e)}
              value={this.state.name}
              //autocomplete="off"
              //required
            />
            {
              nameErr.status &&
              <p className="error">* {nameErr.value}</p>
            }
          </div>
          <div className="form-group">
            <Label>Mobile Number *</Label>
            <input
              type="text"
              name="mobile"
              // maxLength={10}
              // pattern="[0-9]*$"
              onChange={(e) => this.handleChangeInMobile(e)}
              value={this.state.mobile}
              required
              //autocomplete="off"
            />
            {
              mobileErr.status &&
              <p className="error">* {mobileErr.value}</p>
            }
          </div>
          <div className="form-group">
            <Label>Date of Birth *</Label>
            <input
              type="date"
              name="dob"
              max="9999-12-31"
              value={this.state.dob}
              onInput={(e) => this.handleTextChange(e)}
              required
              //autocomplete="off"
            />
            {
              dobErr.status &&
              <p className="error">* {dobErr.value}</p>
            }
          </div>
          <div className="form-group">
            <Label>Location *</Label>
            <input
              type="text"
              name="location"
              pattern="^[A-Za-z0-9,.]*$"
              onInput={(e) => this.handleTextChange(e)}
              value={this.state.location}
              required
            />
            {
              locationErr.status &&
              <p className="error">* {locationErr.value}</p>
            }
          </div>
          <div className="form-group">
            <Label>Gender *</Label>
            <input 
              type="radio" 
              name="gender" 
              id="male" 
              value="male" 
              onChange={(e) => this.handleGenderChange(e)}
            />
            <label htmlFor="male">Male</label>
            <input 
              type="radio" 
              name="gender" 
              id="female" 
              value="female" 
              onChange={(e) => this.handleGenderChange(e)}
            />
            <label htmlFor="female">Female</label>
          </div>
          <div className="form-group">
            <Label>Referral Code</Label>
            <input
              type="text"
              name="referral"
              pattern="^[a-zA-Z0-9!#@]*$"
              onInput={(e) => this.handleTextChange(e)}
              value={this.state.referral}
              required
              //autocomplete="off"
            />
          </div>
          <div className="form-group">
            <Label>Occupation *</Label>
            <input
              type="text"
              name="occupation"
              pattern="^[a-zA-Z0-9!#@]*$"
              onInput={(e) => this.handleTextChange(e)}
              value={this.state.occupation}
              required
            />
            {
              occupationErr.status &&
              <p className="error">* {occupationErr.value}</p>
            }
          </div>
          <div className="form-group">
            <input  
              type="checkbox" 
              name="termsAndCondition" 
              id="terms" 
              onChange={this.handleCheckboxChange}
            />
            <label for="terms">Terms and Condition</label>
            {
              termsAndCondnErr.status &&
              <p className="error">* {termsAndCondnErr.value}</p>
            }
          </div>
          <div className="form-group" style={{textAlign: 'center'}}>
            <Button
              onClick={this.onSubmit}
              primary
              disabled={this.props.disabled}
            >
              SUBMIT
            </Button>
          </div>
        </div>
      </React.Fragment>
      // </form>
    )
  }
}

export default SignupForm