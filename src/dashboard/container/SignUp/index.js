import React from 'react'
import './signup.scss'
import ModalBox from 'Components/modal/modalBox'
import SignUpForm from "./signup-form"
import SignIn from "./../SignIn"
import { mountModal, unMountModal } from 'Components/modal/utils'
import {createSession} from "Utils/session-utils"
import {apiUrl} from "Utils/static-urls"
import {webBaseUrl} from "Utils/static-urls"

export default function SignUp(data) {
  return class SignUp extends React.Component {

    constructor() {
      super()

      this.state = {
        signingUp: false
      }
      //this.apiUrl = "https://businessdhronacharya.000webhostapp.com"
      this.handleSignUp = this.handleSignUp.bind(this)
      this.sendSms = this.sendSms.bind(this)
    }

    sendSms() {
      const signupFormData =  this.signupForm.getData()
      const mobile = signupFormData.mobile
      const message = 'Thank you for showing interest in Business Dhronacharya Institute of Entrepreneurship. Please check your email for further details.'
      fetch(`https://api.textlocal.in/send?apiKey=5+oUTZoIMoc-OGxwx5nlfOPq5pV82EQuUTd8wACwYz&sender=DHRONA&numbers=${mobile}&message=${message}`)
      .then((response) => {
        console.log("Successfully sent sms")
      })
      .catch((error) => {
        console.log("Error in sending sms", error)
      })  
    }

    handleLogin(dataObj) {
      const fetchOptions = {
        method: 'post',
        headers: {
          'Accept': 'application/json',
        },
        body: JSON.stringify(dataObj)
      }
      this.setState({signingUp: true})
      fetch(`${apiUrl}/api/Product/signup.php`, fetchOptions)
      .then((response) => {
        if(response.ok) {
          response.json().then(
            (data) => {
              const url= data.message;    
              window.open(url, 'Download');
              this.sendSms()
              createSession({
                username: dataObj.email,
                password: dataObj.password
              })
              location.href=`${webBaseUrl}/`
              unMountModal()
            },
            () => {
              console.log("Error in sign up")   
            }
          );
          this.setState({signingUp: false})
        } else {
          alert("Username already exist")
          unMountModal()
          mountModal(SignIn({}))
          console.log("Error in sign up") 
          this.setState({signingUp: false})
        }
      })
      .catch((error) => {
        console.log("Error in signup", error)
        this.setState({signingUp: false})
      })
    }

    handleSignUp(e) {
      //e.preventDefault()
      const signupFormData =  this.signupForm.getData()
      if ( signupFormData.name.length &&
          signupFormData.mobile.length &&
          signupFormData.gender.length &&
          signupFormData.location.length &&
          signupFormData.dob.length &&
          signupFormData.occupation.length &&
          signupFormData.termsAndCondition
        ) {
          const payload = {
            email : data.email,
            password : data.password,
            name : signupFormData.name,
            gender: signupFormData.gender,
            dob: signupFormData.dob,
            referalcode: signupFormData.referral,
            location: signupFormData.location,
            occupation : signupFormData.occupation,
            phoneNumber: signupFormData.mobile,
            loginType: data.loginType ? data.loginType : "3",
            loginId: data.loginId ? data.loginId : "manual"
          }
          this.setState({signingUp: true})
          this.handleLogin(payload)
      }
    }

    render() {
      return (
        <ModalBox>
          <div id="signup">
            <SignUpForm 
              ref={(node) => { this.signupForm = node }} 
              onSubmit={this.handleSignUp}
              diabled={this.state.signingUp}
            />
          </div>
        </ModalBox>
      )
    }
  }
}
