import React from 'react'
import './signin.scss'
import ModalBox from 'Components/modal/modalBox'
import Label from "Components/label"
import Button from "Components/button"
import "Sass/form-input.scss"
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props"
import GoogleLogin from "react-google-login"
import { unMountModal, mountModal } from 'Components/modal/utils'
import SignUp from './../SignUp'
import {apiUrl} from "Utils/static-urls"
import {createSession} from "Utils/session-utils"
import {webBaseUrl} from "Utils/static-urls"

export default function SignIn(data) {
  return class SignIn extends React.Component {
    constructor() {
      super()
      this.state = {
        email: "",
        password: "",
        showSignUpModal: false,
        loggingIn: false
        //isLoggedIn: false
      }
      //this.apiUrl = "https://businessdhronacharya.000webhostapp.com"
      this.handleLogin = this.handleLogin.bind(this)
      this.handlePasswordChange = this.handlePasswordChange.bind(this)
      this.handleEmailChange = this.handleEmailChange.bind(this)
      this.mountSignUp = this.mountSignUp.bind(this)
      this.handleSignUp = this.handleSignUp.bind(this)
      this.signIn = this.signIn.bind(this)
      //this.signUpUsingSocialMedia = this.signUpUsingSocialMedia.bind(this)
      this.responseFacebook = this.responseFacebook.bind(this)
      this.googleSuccessResponse = this.googleSuccessResponse.bind(this)
    }

    mountSignUp() {
      this.setState({ showSignUpModal: true })
    }

    signIn(payload) {
      // const payload = {
      //   email : this.state.email,
      //   password : this.state.password,
      // }
      const fetchOptions = {
        method: 'post',
        headers: {
          'Accept': 'application/json',
        },
        body: JSON.stringify(payload)
      }
      console.log("payload", payload)
      fetch(`${apiUrl}/api/Product/signin.php`, fetchOptions)
      .then((response) => {
        console.log("response", response, response.status)
        if(response.status === 200) {
          response.json().then(
            (data) => {
              console.log("if", data)
              createSession(data)
              if(data.isPaidUser === "0") {
                location.href=`${webBaseUrl}/non-paid-user`
              } else {
                location.href=`${webBaseUrl}/paid-user`
              }
              //location.href="/"
              unMountModal()
            }
          )
        } else if(response.status === 401) {
          alert("Wrong password")
        } else {
          alert("New user please signup")
        }
        this.setState({loggingIn: false})
      })
      .catch((error) => {
        console.log("Error in signIn", error)
        this.setState({loggingIn: false})
      })
    }

    handleLogin(e) {
      e.preventDefault()
      const {email, password} = this.state
      if(email.length && password.length) {
        console.log("handle login")
        this.setState({loggingIn: true})
        this.signIn({
          email : this.state.email,
          password : this.state.password
        })
        //unMountModal()
        // var url='file.pdf';    
        // window.open(url, 'Download');  
      }
    }

    responseFacebook(response) {
      console.log("fb res", response)
      if(response.userID) {
        if(!this.state.showSignUpModal) {
          this.signIn({
            email: response.email,
            password: "password"
          })
        } else {
          console.log("response", response)
          let profileInfo = {
            email: response.email,
            password: "password",
            loginType: "FB",
            loginId: "1"
          }
          unMountModal()
          mountModal(SignUp(profileInfo))
          //this.signUpUsingSocialMedia()
        }
      }
    }

    googleSuccessResponse(response) {
      console.log("google success", response)
      if(!this.state.showSignUpModal) {
        this.signIn({
          email: response.profileObj.email,
          password: "password"
        })
      } else {
        console.log("response", response)
        let profileInfo = {
          email: response.profileObj.email,
          password: "password",
          loginType: "Google",
          loginId: "2"
        }
        unMountModal()
        mountModal(SignUp(profileInfo))
      }
    }

    googleFailureResponse(response) {
      console.log("google failure", response)
    }

    handlePasswordChange(evt) {
      const { password } = this.state
      const value = (evt.target.validity.valid || evt.target.validity.valueMissing) ? evt.target.value : eval((evt.target.name));
      this.setState({ [evt.target.name]:  value});
    }
  
    handleEmailChange(evt) {
      this.setState({ [evt.target.name]:  evt.target.value});
    }

    handleSignUp() {
      let profileInfo = {
        email: this.state.email,
        password: this.state.password
      }
      unMountModal()
      mountModal(SignUp(profileInfo))
    }

    render() {
      const googleClientId="578351026464-t1ihhncff7uii99cbk3usc785p8u6srt.apps.googleusercontent.com"
      const { showSignUpModal } = this.state
      
      return (
        <ModalBox>
          <div id="SignIn">
            <form>
              {
                !showSignUpModal &&
                <h2 className="os s2">
                  Sign In / Sign Up 
                </h2>
              }
              {
                showSignUpModal &&
                <h2 className="os s2">
                  Sign Up
                </h2>
              }
              <div className="form">
                <div className="form-group">
                  <Label>Email Id</Label>
                  <input
                    type="text"
                    name="email"
                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                    onInput={this.handleEmailChange.bind(this)}
                    value={this.state.email}
                    autocomplete="off"
                    required
                  />
                </div>
                <div className="form-group">
                  <Label>Password</Label>
                  <input
                    type="password"
                    name="password"
                    // minLength={6}
                    maxLength={12}
                    pattern="^[a-zA-Z0-9!#@_=-.]*$"
                    onInput={this.handlePasswordChange.bind(this)}
                    value={this.state.password}
                    required
                    autocomplete="off"
                  />
                </div>
                <div className={`form-group button-grp ${showSignUpModal ? 'signup' : undefined}`} style={{ textAlign: "center" }}>
                  
                  {
                    !showSignUpModal && 
                    <React.Fragment>
                      <Button
                        onClick={this.handleLogin}
                        primary
                        disabled={this.state.loggingIn}
                      >
                        Sign In
                      </Button>
                      <p onClick={this.mountSignUp} className="os s9">Don't have account? Create Account</p>
                    </React.Fragment>
                  }
                  {
                    showSignUpModal &&
                    <Button
                      onClick={this.handleSignUp}
                      primary
                    >
                      Sign Up
                    </Button>
                  }
                </div>
              </div>
            </form>
            <div className="seperator">
              <span className="line-center">or</span>
            </div>
            <div className="social">
              <FacebookLogin
                appId="2289742744575501"
                fields="name,email,picture"
                callback={this.responseFacebook} 
                render={renderProps => (
                  <Button facebook onClick={renderProps.onClick}>Sign in with Facebook</Button>
                )}
              />
            </div>
            <div className="social">
              <GoogleLogin
                clientId={googleClientId}
                onSuccess={this.googleSuccessResponse}
                onFailure={this.googleFailureResponse}
                render={renderProps => (
                  <Button google onClick={renderProps.onClick}>Sign in with Google</Button>
                )}
              />
            </div>
          </div>
        </ModalBox>
      )
    }
  }
}
//export default SignIn
