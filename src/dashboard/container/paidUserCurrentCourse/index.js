import React from "react"
import "./paidUserCurrentCourse.scss"
import Layout from "Components/layout"
import Select from "Components/select"
import ReactPlayer from "react-player"

class PaidUserCurrentCourse extends React.Component {
  constructor() {
    super()

    this.state = {
      selectedCourseOptionIdx: 0,
      selectedCourseOption: ""
    }

    this.courseOptions = [
      { text: "Current course", value: 1 },
      { text: "Completed course", value: 2 },
      { text: "Full course", value: 3 }
    ]

    this.handleSelectChange = this.handleSelectChange.bind(this)
    this.triggerEndedCurrentCourse = this.triggerEndedCurrentCourse.bind(this)
  }

  handleSelectChange(e) {
    this.setState({
      selectedCourseOptionIdx: parseInt(e.target.value),
      selectedCourseOption: (e.target.text)
    })
  }

  triggerEndedCurrentCourse() {
    console.log("trigger ended current course")
  }

  render() {
    return (
      <div id="paidUserCurrentCourse">
        <div className="adminPortalCurrent">
          <h2 className="os s0">Diploma in Entrepenuership</h2>
        </div>
        <Layout>
          <div className="header">
            Topic: Class 2- business management
          </div>
          {/* <div>
            <Select
              options={this.courseOptions}
              name="courseOptions"
              // placeholder="Pick an option"
              value={this.state.selectedCourseOptionIdx}
              onChange={e => this.handleSelectChange(e)}
            />
          </div> */}
          <div className="current-course">
            <ReactPlayer
              url="https://vimeo.com/309049643"
              className='react-player'
              playing={false}
              onEnded={this.triggerEndedCurrentCourse}
            />
          </div>
        </Layout>
      </div>
    )
  }
}

export default PaidUserCurrentCourse