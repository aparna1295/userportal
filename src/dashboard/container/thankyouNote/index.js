import React from "react"

class ThankyouNote extends React.Component {
  render() {
    return(
      <div 
        style={{
          height: 'calc(100vh - 76px - 170px)', 
          display: 'flex', 
          alignItems: 'center', 
          justifyContent: 'center', 
          margin: '20px'
        }}
        className="os s5"
      >
        We have sent you an email with the coupon code to enroll.
      </div>
    )
  }
}

export default ThankyouNote