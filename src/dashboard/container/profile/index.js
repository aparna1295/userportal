import React from "react"
import Icon from "Components/icons"
import "./profile.scss"

class Profile extends React.Component {
  constructor() {
    super()
    this.state = {
      userDetails: [],
      username: ""
    }
  }

  componentDidMount() {
    this.setState({
      userDetails: JSON.parse(localStorage.getItem("userDetails")),
      username: localStorage.getItem("username")
    })
  }

  render() {
    const {userDetails} = this.state
    return (
      <div id="profile">
        <div className="profile-header" style={{backgroundImage: 'url(./images/paid-user.jpg)'}}>
          <h2 className="os s1">Welcome {this.state.username}</h2>
        </div>
        <div className="container">
          <h3 className='sub-header os s3'>Your Details</h3>
          <div className="content">
            <p>Name: <span>{userDetails.name}</span></p>
            <p>Location: <span>{userDetails.location}</span></p>
            <p>Email: <span>{userDetails.email}</span></p>
            <p>Phone: <span>{userDetails.phoneNumber}</span></p>
            <p>Occupation: <span>{userDetails.occupation}</span></p>
            <div className="footer">
              <div>
                <Icon name="help" />
                <div>Help</div>
              </div>
              <div>
                <Icon name="faq" />
                <div>FAQ</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Profile