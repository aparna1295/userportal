import React from "react"
import "./course-detail.scss"
import Icon from "Components/icons"
import Button from "Components/button"
import Layout from "Components/layout"
import ReactPlayer from "react-player"
import * as Api from "./../../api"

class Course extends React.Component {
  constructor() {
    super()

    this.state = {
      courseDetail: [],
      enrolling: false
    }

    this.triggerEndedCurrentCourse = this.triggerEndedCurrentCourse.bind(this)
    this.handleEnrollClick = this.handleEnrollClick.bind(this)
    this.successEmailCallback = this.successEmailCallback.bind(this)
  }

  componentDidMount() {
    this.setState({courseDetail: this.props.location.state})
  }

  triggerEndedCurrentCourse() {
    const {courseDetail} = this.state
    this.updateVideoStatus({
      userId: JSON.parse(localStorage.getItem("userDetails")).id,
      videoName: courseDetail.videoid,
      courseId: courseDetail.id,
      videoType: courseDetail.videoTitle,
      videoSeenFully: "1"
    })
  }

  updateVideoStatus(payload) {
    Api.updateVideoStatus(payload)
  }

  sendEmail(payload, successCallback) {
    Api.sendEmail(payload, successCallback)
  }

  handleEnrollClick() {
    this.setState({enrolling: true})
    this.sendEmail({
      emailList: [
        {
          email: JSON.parse(localStorage.getItem("userDetails")).email, 
          name: JSON.parse(localStorage.getItem("userDetails")).name
        }
      ],
      emailType: "4"
    }, this.successEmailCallback)
  }

  successEmailCallback() {
    this.setState({enrolling: false})
    this.props.history.push('/success-enrollment')
  }

  render() {
    const {courseDetail, enrolling} = this.state
    return(
      <div id="courseDetail">
        <div className="course-detail-header" style={{backgroundImage: 'url(./../images/header-image.jpg)'}}>
          <h2 className="os s0">{courseDetail.name}</h2>
        </div>
        <Layout>
          <div className="header">
            Topic: {courseDetail.videoTitle}
          </div>
          <div className="current-course">
            <ReactPlayer
              url={courseDetail.videoid}
              className='react-player'
              playing={false}
              onEnded={this.triggerEndedCurrentCourse}
            />
          </div>
          <p className="os s6">
            {courseDetail.description}
          </p>
          <div className="section-3">
            <div className="img">
              <img src="./../images/course-page10.jpg" />
            </div>
            <div className="img">
              <img src="./../images/course-page9.jpg" />
            </div>
            <div className="img">
              <img src="./../images/course-page8.jpg" />
            </div>
          </div>
          <div className="section-4">
            <Button 
              primary
              onClick={this.handleEnrollClick}
              disabled={enrolling}
            >
              Enroll Now
            </Button>
          </div>
        </Layout>
      </div>
    )
  }
}

export default Course