import React from "react"
import "./courseList.scss"
import Layout from "Components/layout"
import Button from "Components/button"
import * as Api from "./../../api"
import {webBaseUrl} from "Utils/static-urls"

class CourseList extends React.Component {
  constructor() {
    super()

    this.state = {
      courseList: []
    }

    this.fetchCourseListData = this.fetchCourseListData.bind(this)
    this.successCourseListCallback = this.successCourseListCallback.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  componentDidMount() {
    this.fetchCourseListData()
  }

  fetchCourseListData() {
    this.fetchCourseList({}, this.successCourseListCallback)
  }

  successCourseListCallback(courseList) {
    console.log("response", courseList)
    this.setState({courseList})
  }

  fetchCourseList(payload, successCallback) {
    Api.fetchCourseList(payload, successCallback)
  }

  handleClick(item) {
    this.updateVideoStatus({
      userId: JSON.parse(localStorage.getItem("userDetails")).id,
      videoName: item.videoid,
      courseId: item.id,
      videoType: item.videoTitle,
      videoSeenFully: "2"
    })
    this.props.history.push(`/course-detail/${item.id}`, item)
  }

  updateVideoStatus(payload) {
    Api.updateVideoStatus(payload)
  }

  renderCourse(item) {
    return (
      <div className="course">
        <div className="player-image">
          <img src={item.thumbnail} />
        </div>
        <p className="title os s5">{item.name}</p>
        <p className="course-brief-detail os s6">
          {item.shorttext}
        </p>
        <Button 
          primary 
          disabled={item.isVideoActive === "0"}
          onClick={() => this.handleClick(item)}
        >
          {
            item.isVideoActive === "0"
            ? 'Coming Soon..'
            : 'More'
          }
        </Button>
      </div>
    )
  }
  
  render() {
    return (
      <div id="courseList">
        <div className="course-list-header" style={{backgroundImage: 'url(./images/header-image.jpg)'}}>
          <h2 className="os s0">Courses</h2>
        </div>
        <Layout>
          <div className="content">
            {
              this.state.courseList.map((course) => {
                return this.renderCourse(course)
              })
            }
          </div>
        </Layout>
      </div>
    )
  }
}

export default CourseList