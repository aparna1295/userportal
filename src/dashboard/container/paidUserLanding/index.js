import React from "react"
import Icon from "Components/icons"
import "./paidUserLanding.scss"

class PaidUserLanding extends React.Component {
  constructor() {
    super()
  }

  render() {
    return (
      <div id="paidUserLanding">
        <div className="content">
          <h2 className="os s1">Welcome Nishanth</h2>
          <h3 className="os s1">Welcome to Business Dhronacharya What would you like to do today</h3>
          <div className="user-profile">
            <div>
              <Icon name="logout" />
              <p>Your Profile</p>
            </div>
            <div>
              <Icon name="logout" />
              <p>Courses</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default PaidUserLanding