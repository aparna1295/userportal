import { GET, POST } from 'Utils/fetch'
//import { apiUrl } from 'Utils/static-urls';

export function fetchCourseList(payloadObj, successCallback) {
	return POST({
		api: '/api/admin/CourseList.php',
		apiBase: 'businessdhronacharya',
		data: payloadObj,
		handleError: true
	})
		.then((response) => {
			console.log("re", response)
			successCallback(response.Courses.records)
			// response.json().then((data) => {
			//   successCallback(data)
			// })
			// Notify("success", "Successfully created or updated stock")
			// setTimeout(() => {
			//     location.href = `/admin/stock-and-price/list?retailerId=${payloadObj.inventories[0].retailer_id}&outletName=${payloadObj.inventories[0].outlet_name}&stateId=${payloadObj.inventories[0].state_id}&selectedGenreIdx=${payloadObj.inventories[0].genre_id}`
			// }, 500)
		})
		.catch(err => {
			console.log("Error in fetching course list", err)
			// err.response.json().then(json => { Notify("danger", json.message) })
			// failureCallback()
		})
}

export function updateVideoStatus(payloadObj) {
	return POST({
		api: '/api/Product/UpdateUserVideo.php',
		apiBase: 'businessdhronacharya',
		data: payloadObj,
		handleError: true
	})
		.then((response) => {
			console.log("Successfully updated video status")
		})
		.catch(err => {
			console.log("Error in updating video status", err)
			// err.response.json().then(json => { Notify("danger", json.message) })
			// failureCallback()
		})
}

export function sendEmail(payloadObj, successCallback) {
	return POST({
		api: '/api/Product/sendEmail.php',
		apiBase: 'businessdhronacharya',
		data: payloadObj,
		handleError: true
	})
		.then((response) => {
			successCallback()
			console.log("Successfully sent email")
		})
		.catch(err => {
			console.log("Error in sending email", err)
			// err.response.json().then(json => { Notify("danger", json.message) })
			// failureCallback()
		})
}