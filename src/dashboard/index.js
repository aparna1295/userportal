import React from 'react'
import { Provider } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import { Router } from 'react-router'
import configureStore from './store/configureStore'
import rootSaga from './middleware/saga'
import Header from 'Components/header'
import Footer from 'Components/footer'
import Landing from "./container/landing"
import Layout from 'Components/layout';

const config = configureStore()
config.runSaga(rootSaga)

class Root extends React.Component {
  constructor() {
    super()

    this.state ={
      isLoggedIn: false
    }
  }

  componentDidMount() {
    if(localStorage.getItem("username")) {
      this.setState({isLoggedIn: true})
    }
  }

  render() {
    return (
      <Provider store={config.store}>
      <Router history={history}>
        <div style={{
          backgroundColor: '#fff',
          width: '100%',
          //maxWidth: '1440px',
          margin: '0 auto',
          height: '100vh',
          overflow: 'auto'
        }}>
          <Header />
          <Route path='/' component={Landing} />
          <Route path='/paid-user' component={Landing} /> 
          <Footer />
          {/* <Switch>
            <Route exact path="/home" component={Landing} />
          </Switch> */}
        </div>
      </Router>
    </Provider>
    )
  }
}

export default Root
